package com.siteassist.webcrawler.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class WebTree<T> {

    T value;

    List<WebTree<T>> children = new LinkedList<>();

    public WebTree(T value) {
        this.value = value;
    }

    public WebTree(T value, List<WebTree<T>> children) {
        this.value = value;
        this.children = children;
    }

    public void setChildren(List<WebTree<T>> children) {
        this.children = children;
    }

    public T getValue() {
        return value;
    }

    public List<WebTree<T>> getChildren() {
        return children;
    }

    public void addChildWebTree(WebTree<T> childTree) {
        children.add(childTree);
    }

    public void printBFS() {
        Queue<WebTree<T>> queue = new LinkedList<>();
        queue.offer(this);
        while (!queue.isEmpty()) {
            WebTree<T> current = queue.poll();
            System.out.print(current.value + " ");
            queue.addAll(current.children);
        }
    }

    public void printDFS() {
        System.out.print(value + " ");
        for (WebTree<T> child : children) {
            child.printDFS();
        }
    }

}
