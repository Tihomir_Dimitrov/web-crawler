package com.siteassist.webcrawler.services;

import com.siteassist.webcrawler.domain.WebTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class RelativeCrawlerService {

    public static final String REGEX_FOR_RELATIVE_LINKS = "^\\/[\\w:\\/.#-]*";
    public static final String REGEX_FOR_HYPERLINKS = "href=\"[\\w\\.\\-#\\:\\/]*\"";

    private final RestTemplate restTemplate;

    @Autowired
    public RelativeCrawlerService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public WebTree<String> traverseWebpage(String startingWebpage) throws IOException {
        isValidWebpage(startingWebpage);

        final WebTree<String> webTree = new WebTree<>(startingWebpage);

        buildWebTreeNode(webTree);

        return webTree;
    }

    private void buildWebTreeNode(WebTree<String> webRoot) {
        Queue<WebTree<String>> queue = new LinkedList<>();
        queue.offer(webRoot);

        final Set<String> uniquePagesVisited = new HashSet<>();
        uniquePagesVisited.add(webRoot.getValue());

        while (!queue.isEmpty()) {
            WebTree<String> currentWebTree = queue.poll();

            List<WebTree<String>> childPages = getChildPages(currentWebTree, uniquePagesVisited, webRoot.getValue());

            if (ObjectUtils.isEmpty(childPages))
            {
                continue;
            }

            queue.addAll(currentWebTree.getChildren());
        }

    }

    private List<WebTree<String>> getChildPages(final WebTree<String> currentWebTree,
                                                final Set<String> uniquePagesVisited,
                                                final String webRoot)
    {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(currentWebTree.getValue(), String.class);

        List<String> hyperlinks = getHyperlinks(responseEntity);

        for (String hyperlink : hyperlinks)
        {

            Optional<String> validatedIsRelativeLink = validateAndBuildRelativeLink(hyperlink, webRoot);


            if (validatedIsRelativeLink.isEmpty())
            {
                continue;
            }

            if (uniquePagesVisited.contains(validatedIsRelativeLink.get()))
            {
                continue;
            }

            if (!isValidWebpage(validatedIsRelativeLink.get()))
            {
                continue;
            }


            WebTree<String> childWebpage = new WebTree<>(validatedIsRelativeLink.get());

            currentWebTree.addChildWebTree(childWebpage);
            uniquePagesVisited.add(validatedIsRelativeLink.get());
        }

        return currentWebTree.getChildren();
    }

    private List<String> getHyperlinks(ResponseEntity<String> responseEntity) {
        Pattern pattern = Pattern.compile(REGEX_FOR_HYPERLINKS);
        Matcher matcher = pattern.matcher(responseEntity.getBody());

        List<String> urls = new ArrayList<>();
        while (matcher.find()) {
            String parameter = matcher.group();
            urls.add(parameter.substring(6, parameter.length()-1));
        }

        return urls;
    }

    private Optional<String> validateAndBuildRelativeLink(String url, String rootPage) {
        String webRootToRegex = rootPage;
        List<String> symbolsToEscape = List.of("/", ".", "-", "_", "#", "%", "^");
        symbolsToEscape.forEach(symbol -> webRootToRegex.replaceAll(symbol, "\\" + symbol));

        Pattern firstPattern = Pattern.compile(webRootToRegex);
        Matcher firstMatcher = firstPattern.matcher(url);

        List<String> urls = new ArrayList<>();
        while (firstMatcher.find()) {
            String parameter = firstMatcher.group();
            urls.add(parameter);
        }

        Pattern secondPattern = Pattern.compile(REGEX_FOR_RELATIVE_LINKS);
        Matcher secondMatcher = secondPattern.matcher(url);

        while (secondMatcher.find()) {
            String parameter =  secondMatcher.group();
            urls.add(rootPage + parameter.substring(1));
        }

        if (urls.isEmpty())
        {
            return Optional.empty();
        }
        else
        {
            return Optional.of(urls.get(0));
        }
    }

    private boolean isValidWebpage(String webpage) {
        try
        {
            restTemplate.getForEntity(webpage, Void.class);
            return true;
        }
        catch (RestClientException e)
        {
            System.out.printf("Failed check for valid webpage: %s%n", webpage);
            return false;
        }
    }

}
