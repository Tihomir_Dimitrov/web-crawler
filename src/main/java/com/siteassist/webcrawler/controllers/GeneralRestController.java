package com.siteassist.webcrawler.controllers;

import com.siteassist.webcrawler.domain.WebTree;
import com.siteassist.webcrawler.services.RelativeCrawlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;


@RestController
public class GeneralRestController {

    private final RelativeCrawlerService relativeCrawlerService;

    @Autowired
    public GeneralRestController(final RelativeCrawlerService relativeCrawlerService) {
        this.relativeCrawlerService = relativeCrawlerService;
    }

    @GetMapping("/craw-relative")
    public WebTree<String> traverseRelativeWebLinks(@RequestParam String webpage)
    {
        try
        {
            return relativeCrawlerService.traverseWebpage(webpage);
        }
        catch (final IllegalArgumentException e)
        {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        catch (final IOException e)
        {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
